// Définition des arrays constantes avec en indice 0 le titre, 1 les indices de ligne et 2 les indices de colomnes.
const arrayTitanic = ["Titanic", [
    [0],
    [1],
    [5],
    [4],
    [0]
  ],
  [
    [1],
    [2],
    [2],
    [3],
    [2]
  ]
];
const arrayInvader = ["Invader", [
    [0],
    [3],
    [2],
    [1, 5],
    [2, 2, 1],
    [4, 1],
    [4],
    [4, 1],
    [2, 2, 1],
    [1, 5],
    [2],
    [3],
    [0],
  ],
  [
    [0],
    [1, 1],
    [1, 1],
    [7],
    [2, 3, 2],
    [11],
    [1, 7, 1],
    [1, 1, 1, 1],
    [2, 2],
    [0],
  ]
];
const arraySimplon = ["Simplon", [
    [0],
    [7],
    [9],
    [11],
    [4, 4],
    [4, 4],
    [4, 4],
    [4, 4],
    [4, 4],
    [4, 4],
    [3, 4, 4, 3],
    [3, 4, 4, 3],
    [3, 4, 4, 3],
    [3, 4, 4, 3],
    [3, 4, 4, 3],
    [4, 4],
    [4, 4],
    [4, 4],
    [4, 4],
    [4, 4],
    [4, 4],
    [11],
    [9],
    [7],
    [0],
  ],
  [
    [0],
    [7],
    [9],
    [11],
    [4, 4],
    [4, 4],
    [4, 4],
    [4, 5, 4],
    [4, 5, 4],
    [4, 5, 4],
    [3, 5, 3],
    [3, 3],
    [3, 3],
    [3, 3],
    [3, 5, 3],
    [4, 5, 4],
    [4, 5, 4],
    [4, 5, 4],
    [4, 4],
    [4, 4],
    [4, 4],
    [11],
    [9],
    [7],
    [0],
  ]
]
// solutionTitanic = array de réponse pour titanic
let solutionTitanic = [
  ['0', '0', '1', '0', '0', '0'],
  ['0', '0', '1', '1', '0', '0'],
  ['0', '0', '1', '1', '0', '0'],
  ['0', '1', '1', '1', '0', '0'],
  ['0', '0', '1', '1', '0', '0']
];
// tableau matrice, vide à l'initialisation et qui se remplis en fonction des cliques du joueur
let solutionUserTitanic = [
  ['0', '0', '0', '0', '0', '0'],
  ['0', '0', '0', '0', '0', '0'],
  ['0', '0', '0', '0', '0', '0'],
  ['0', '0', '0', '0', '0', '0'],
  ['0', '0', '0', '0', '0', '0']
];

// définition de la fonction qui génere un tableau HTML en fonction du parametre array.
function generate_table(array) {
  // mainDiv = div dans laquelle la table sera créée
  var mainDiv = document.getElementById('main');

  // création d'un élément <table> et d'un élément <tbody>
  var tbl = document.createElement("table");
  var tblBody = document.createElement("tbody");
  // création des cellules
  for (var i = 0; i < array[2].length + 1; i++) {
    // creates a table row
    let row = document.createElement("tr");
    for (var j = 0; j < array[1].length + 1; j++) {
      // Création d'un élement <td> et d'un texte node. Fait du texte node le contenu du <td>, insère le td à la fin du table row.
      var cell = document.createElement("td");
      var cellText = document.createTextNode("");
      cell.appendChild(cellText);
      row.appendChild(cell);
      // Nous attribuons 2 valeurs ArrayX et ArrayY à toutes nos cellules pour pouvoir les manipuler facilement
      cell.setAttribute("ArrayX", (i - 1));
      cell.setAttribute("ArrayY", (j - 1));
      // Attribution des indices
      if (cell.getAttribute('ArrayX') < 0) {
        cell.innerHTML = (array[1][j - 1]);
        cell.style.backgroundColor = 'darkblue';
        cell.classList.add('topHeading');

      }
      if (cell.getAttribute('ArrayY') < 0) {
        cell.innerHTML = (array[2][i - 1]);
        cell.style.backgroundColor = 'darkblue';
        cell.classList.add('leftHeading');


      }
      cell.addEventListener("click", leftClick);
      cell.addEventListener("contextmenu", rightClick);
      // Appliquer une couleur différente aux cellules comprenant des indices
      if ((cell.getAttribute('ArrayX') < 0) && (cell.getAttribute('ArrayY') < 0)) {
        cell.style.backgroundColor = 'white';
        cell.innerHTML = ('');
      }
    }

    function leftClick(){
      if (this.classList.contains('topHeading') || this.classList.contains('leftHeading')) {
        return;
      } else {
        this.style.backgroundColor = "black";
        // En fonction de la cellule cliquée, remplace la valeur correspondante dans solutionUserTitanic par un "1";
        solutionUserTitanic[this.getAttribute("ArrayX")][this.getAttribute("ArrayY")] = "1";
      }
    }

    function rightClick() {
      if (this.classList.contains('topHeading') || this.classList.contains('leftHeading')) {
        return;
      } else {
        this.style.backgroundColor = "#007BFF";
        // En fonction de la cellule cliquée, remplace la valeur correspondante dans solutionUserTitanic par un "0";
        solutionUserTitanic[this.getAttribute("ArrayX")][this.getAttribute("ArrayY")] = "0";
      }
    }
    tblBody.appendChild(row);

  }
  tbl.appendChild(tblBody);
  mainDiv.appendChild(tbl);
  // var verifEtCoeurs = document.getElementById('verifEtCoeurs');
  // var tBody = document.querySelector('tbody');

}


// La fonction vérification compare les 2 tableaux solutionTitanic
let nbVies = 3;

function verification() {
  if (solutionTitanic.join() === solutionUserTitanic.join()) {
    document.getElementById("buttonVerif").classList.remove('btn-danger');
    document.getElementById("buttonVerif").classList.remove('btn-warning');
    document.getElementById("buttonVerif").classList.add('btn-success');
    document.getElementById("buttonVerif").innerHTML = "Victoire !";
  } else {
    nbVies--;
    switch (nbVies) {
      case 0:
        document.getElementById('coeurs').innerHTML = ('<i class="fas fa-heart-broken  fa-3x  mr-2"></i><i class="fas fa-heart-broken fa-3x  mr-2"></i><i class="fas fa-heart-broken fa-3x  mr-2"></i>');
        document.getElementById("buttonVerif").classList.remove('btn-warning');
        document.getElementById("buttonVerif").classList.add('btn-danger');
        document.getElementById("buttonVerif").innerHTML = "Game Over" + "<br>" + "Nouvel essai dans " + "<div id='countdown'></div>";
        // Timer 
        var timeleft = 3;
        var downloadTimer = setInterval(function () {
          document.getElementById("countdown").innerHTML = (timeleft);
          timeleft -= 1;
          if (timeleft <= 0) {
            clearInterval(downloadTimer);
          }
        }, 1000);
        setTimeout(function () {
          location.reload();
        }, 3000);
        // Fin Timer
        break;
      case 1:
        document.getElementById('coeurs').innerHTML = ('<i class="fas fa-heart fa-3x  mr-2"></i><i class="fas fa-heart-broken fa-3x  mr-2"></i><i class="fas fa-heart-broken fa-3x  mr-2"></i>');
        document.getElementById("buttonVerif").classList.remove('btn-danger');
        document.getElementById("buttonVerif").classList.add('btn-warning');
        document.getElementById("buttonVerif").innerHTML = "Essaye encore";
        break;
      case 2:
        document.getElementById('coeurs').innerHTML = ('<i class="fas fa-heart fa-3x  mr-2"></i><i class="fas fa-heart fa-3x  mr-2"></i><i class="fas fa-heart-broken fa-3x  mr-2"></i>');
        document.getElementById("buttonVerif").classList.remove('btn-danger');
        document.getElementById("buttonVerif").classList.add('btn-warning');
        document.getElementById("buttonVerif").innerHTML = "Essaye encore";
        break;
      case 3:
        document.getElementById('coeurs').innerHTML = ('<i class="fas fa-heart  fa-3x  mr-2"></i><i class="fas fa-heart fa-3x  mr-2"></i><i class="fas fa-heart fa-3x  mr-2"></i>');

        break;
    }


  }
}

document.getElementById("difficultyEasy").addEventListener("click", function () {
  document.getElementById("buttonVerif").innerHTML = "Vérifier";
  document.getElementById('main').innerHTML = '';
  document.getElementById('reglesId').classList.add('d-none');
  document.getElementById('verifEtCoeurs').classList.remove('d-none');
  main.style.lineHeight = "60px";
  generate_table(arrayTitanic);
});

document.getElementById("difficultyNormal").addEventListener("click", function () {
  document.getElementById("buttonVerif").innerHTML = "Vérifier";
  document.getElementById("buttonVerif").classList.remove('btn-success');
  document.getElementById("buttonVerif").classList.add('btn-danger');
  document.getElementById('main').innerHTML = '';
  document.getElementById('reglesId').classList.add('d-none');
  document.getElementById('verifEtCoeurs').classList.remove('d-none');
  main.style.lineHeight = "60px";
  generate_table(arrayInvader);
});

document.getElementById("difficultyHard").addEventListener("click", function () {
  document.getElementById("buttonVerif").innerHTML = "Vérifier";

  document.getElementById("buttonVerif").classList.remove('btn-success');
  document.getElementById("buttonVerif").classList.add('btn-danger');
  document.getElementById('main').innerHTML = '';
  document.getElementById('reglesId').classList.add('d-none');
  document.getElementById('verifEtCoeurs').classList.remove('d-none');

  generate_table(arraySimplon);
  var arraytd = document.querySelectorAll('table td');
  for (i = 0; i < arraytd.length; i++) {
    arraytd[i].style.height = "55px";
    arraytd[i].style.width = "55px";
  }
  main.style.lineHeight = "55px";

});
document.getElementById("regles").addEventListener("click", function () {
  document.getElementById('main').innerHTML = '';
  document.getElementById('reglesId').classList.remove('d-none');
  document.getElementById('verifEtCoeurs').classList.add('d-none');
  main.style.lineHeight = "60px";
});
var btn = document.getElementById('buttonVerif');
btn.addEventListener("click", verification);


// function setStyleOn(rule, targetId, value) {
//   for (i = 0; i < targetId.length; i++) {
//     x = document.getElementsByTagName(targetId)[i];
//     x.style[rule] = value + 'em';
//     x.style.height = value + 'em';

//   }
// }