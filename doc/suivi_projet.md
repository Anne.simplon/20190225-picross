# Projet Picross : Anne & Tommy
#### Début : 25/02/19


# Etape 1
#### Création de la page html (bootstrap)


# Etape 2
#### Création du tableau statique "Titanic"
- Function generate_table(array) qui crée une table HTML en fonction de l'array selectionnée en parametre.
- Ajout d'un attribut arrayX et arrayY pour chaque case (élement TD de notre table);
- Ajout des indices dans les cases de première ligne et la première colonne.
- Création d'un Array solutionTitanic qui comprend le résultat final.
- Création d'un Array solutionUserTitanic vide et qui se remplira en fonction des cliques de l'utilisateur.

# Etape 4 
## Mise en place des fonctionnalités
#### leftClick()
- La fonction leftClick change le background color (noir) de la cellule cliquée si elle ne fais pas partie de nos headers d'indices. De plus, lorsque l'utilisateur clique dans une cellue de la table HTML, l'array solutionUser change en conséquence (ajout d'un "1" à la place correspondante). 
- La fonction rightClick change le background color (noir) de la cellule cliquée si elle ne fais pas partie de nos headers d'indices. De plus, lorsque l'utilisateur clique droit dans une cellue de la table HTML, l'array solutionUser change en conséquence (ajout d'un "0" à la place correspondante). 
- Fonction Vérification : le script compare la valeurs des deux Array solution et solutionUser. Si elles sont identiques, un message de victoire est affiché. Sinon, une vie est perdue.

#### rightClick()

# Etape 4
#### Création des boutons

# Etape 5
#### Ecriture des règles du jeu
